import uvicorn

from fastapi import FastAPI, WebSocket, Request
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates


import asyncio
import asyncio_redis


app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")


templates = Jinja2Templates(directory="templates")


async def get_redis_pool():
    try:
        pool = await asyncio_redis.Pool.create(host='localhost', port=6379)
        return pool
    except Exception as e:
        print(e)
        return None


@app.get("/")
async def get(request: Request):
    return templates.TemplateResponse("index.html", {"request": request, "id": 1})


@app.websocket("/temps")
async def temps_websocket(websocket: WebSocket):
    await websocket.accept()
    pool = await get_redis_pool()
    subscriber = await pool.start_subscribe()
    await subscriber.subscribe(["temperatures"])
    ws_connected = True
    while pool and ws_connected:
        try:
            reply = await subscriber.next_published()
            await websocket.send_text(reply.value)
        except Exception as e:
            print(e)
            ws_connected = False
    pool.close()

@app.post("/temps/{sensor}/{temp}")
async def set_sensor_temp(sensor, temp):
    return {"sensor": sensor, "temp":temp}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=int(os.getenv("PORT", 8000)))
